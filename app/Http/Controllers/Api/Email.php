<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Libraries\Broker;
use App\Http\Controllers\Controller;
use App\User;

class Email extends Controller
{
    
    public function email($id){
    	$data = User::select('id','email')
    			->where('id','!=',$id)
    			->get();
		$test = [];
		$statusCode = 0;

        if ($data) {
            $statusCode = 200;
            foreach ($data as $datas) {
                $array = [
                    'id'=> $datas->id,
                    'email'  => $datas->email
                ];
                array_push($test, $array);
            }

            $response =  [
                'data'  => $test
            ];

        }
        else{
            $response = 'Data Null';
            $statusCode = 400;
        }
        return response()->json($response, $statusCode);
    }

    public function getEmail($id){
        $data = User::select('id','email')
                ->find($id);
        $statusCode = 0;

        if ($data) {
            $statusCode = 200;
            $array = array(
                'id'=> $data->id,
                'email'  => $data->email
            );

            $response =  [
                'data'  => $array
            ];

        }
        else{
            $response = 'Data Null';
            $statusCode = 400;
        }
        return response()->json($response, $statusCode);   
    }
}
