
<div class="left main-sidebar">

	<div class="sidebar-inner leftscroll">

		<div id="sidebar-menu">
    
			<ul>
                <!-- <li class="submenu">
                    <a href="#" class="active subdrop"><i class="fa fa-fw fa-file-text-o"></i> <span> Forms </span> <span class="menu-arrow"></span></a>
                        <ul class="list-unstyled" style="display: block;">
                            <li><a href="forms-general.html">General Elements</a></li>
                            <li><a href="forms-select2.html">Select2</a></li>
                            <li><a href="forms-validation.html">Form Validation</a></li>
                        </ul>
                </li> -->
					
                <li class="submenu">
                    <a href="{{ route('load') }}"><i class="fa fa-envelope-opens bigfonts"></i><span> User </span> </a>
                </li>
                <li class="submenu">
                    <a href="{{ route('add') }}"><i class="fa fa-envelope-opens bigfonts"></i><span> Add User </span> </a>
                </li>

            </ul>

            <div class="clearfix"></div>

		</div>
    
		<div class="clearfix"></div>

	</div>

</div>