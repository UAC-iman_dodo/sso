<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/server', 'Host@index')->name('index');
Route::get('/server/login', 'Host@showLoginForm')->name('log');
Route::post('/server/go', 'Host@login')->name('login');
Route::get('/server/logout', 'Host@logout')->name('logout');


Route::get('/', 'Users@load');

Route::get('/load', 'Users@load')->name('load');

Route::get('/add', 'Users@add')->name('add');
Route::post('/insert', 'Users@insert')->name('insert');

Route::get('/edit/{id}', 'Users@edit')->name('edit');
Route::post('/update/{id}', 'Users@update')->name('update');

Route::get('/delete/{id}', 'Users@delete')->name('delete');
