<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Level;
use Carbon\Carbon;
use DB;

class Users extends Controller
{

    public function load(){
    	$user = User::leftJoin('levels','levels.id_level','=','users.level_id')
                    ->where('deleted_at','=',null)
                    ->get();
    	return view('views',compact('user'));
    }

    public function add(){
        $levels = Level::get();
        $level = array();
        foreach ($levels as $row) {
            $level[$row->id_level] = $row->level;
        }
        $action = route('insert');
        $subtitle = "Tambah user";
        return view('crud', compact('subtitle', 'action', 'level'));
    }

    public function insert(Request $request){
        DB::beginTransaction();
        try {  
            $user = new User; 
        
            $user->name 	= $request->input('name');
            $user->email 	= $request->input('email');
            $user->password = bcrypt($request->input('password'));
            $user->no_telp 	= $request->input('no_telp');
            $user->level_id 	= $request->input('level');

            $user->save();

            DB::commit();
        }catch (\Exception $e) {
            DB::rollback();
            return $e->getMessage();
        }
        return redirect()->route('load');
    }

    public function edit($id){
        $user = User::find($id);
        $levels = Level::get();
        $level = array();
        foreach ($levels as $row) {
            $level[$row->id_level] = $row->level;
        }
        $action = route('update', ['id'=>$id]);
        $subtitle = "Edit user";
        return view('crud',compact('user', 'subtitle', 'action', 'level'));
    }

    public function update(Request $request, $id){
        DB::beginTransaction();
        try {  
        	$user = User::find($id);

            $user->name 	= $request->input('name');
            $user->email 	= $request->input('email');
            // $user->password = bcrypt($request->input('password'));
            $user->no_telp 	= $request->input('no_telp');
            $user->level_id = $request->input('level');

        	$user->save();

            DB::commit();
        }catch (\Exception $e) {
            DB::rollback();
            return $e->getMessage();
        }
        return redirect()->route('load');
	}

    // public function delete($id){
    //     $user = User::find($id);
    //     $user->deleted_at = Carbon::now();
    //     $user->save();

    //     return redirect()->route('load');
    // }
}
