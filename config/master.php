<?php

return [
    'host_url' => env('UA_HOST_URL', 'http://localhost:8000/server'),
    'broker_id' => env('UA_BROKER_ID', 4215552),
    'broker_secret' => env('UA_BROKER_SECRET', 'c6ff767473d05b18982074f0826c4d1a55f2fffb'),
    'uri' => [
        'profile' => 'profile',
        'login' => 'login',
        'logout' => 'logout',
    ]
];