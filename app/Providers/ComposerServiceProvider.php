<?php



namespace App\Providers;



use Illuminate\Support\ServiceProvider;

use Illuminate\Support\Facades\View;

use App\Libraries\Broker;



class ComposerServiceProvider extends ServiceProvider

{

    /**

     * Bootstrap services.

     *

     * @return void

     */

    public function boot()

    {

        View::composer('tempt', 'App\Libraries\Profile');

    }



    /**

     * Register services.

     *

     * @return void

     */

    public function register()

    {

        //

    }

}