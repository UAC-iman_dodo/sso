@extends('tempt')

@section('content')

<div class="content">
    
	<div class="container-fluid">
			<div class="row">
				<div class="col-xl-12">
						<div class="breadcrumb-holder">
                                <h1 class="main-title float-left">User</h1>
                                <ol class="breadcrumb float-right">
									<li class="breadcrumb-item active">List User</li>
                                </ol>
                                <div class="clearfix"></div>
                        </div>
				</div>
			</div>

			<div class="row">
			
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">						
					<div class="card mb-3">
						<div class="card-header">
						</div>
							
						<div class="card-body">
							<div class="table-responsive">
							<table id="example1" class="table table-bordered table-hover display">
								<thead>
									<tr>
										<th>Nama</th>
										<th>Email</th>
										<th>Level</th>
										<th>Nomor telepon</th>
										<th></th>
									</tr>
								</thead>							
								<tbody>
									@foreach ($user as $users)
										<tr>
											<td>{{ @$users->name }}</td>
											<td>{{ @$users->email }}</td>
											<td>{{ @$users->level }}</td>
											<td>{{ @$users->no_telp }}</td>
											<td>
												<a href="{{ route('edit', ['id'=>$users->id]) }}" class="btn btn-primary">Edit</a>
												<!-- <a href="{{ route('delete', ['id'=>$users->id]) }}" class="btn btn-primary">delete</a> -->
											</td>
										</tr>
									@endforeach
								</tbody>
							</table>
							</div>
							
						</div>														
					</div><!-- end card-->					
				</div>

			</div>

    </div>
	<!-- END container-fluid -->

</div>
@endsection