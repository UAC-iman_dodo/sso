<?php

namespace App\Libraries;

use Illuminate\View\View;
use App\Libraries\Broker;

class Profile
{
    protected $broker;

    public function __construct(Broker $broker)
    {
        $this->broker = $broker;
    }

    public function compose(View $view)
    {
        $view->with([
            'broker_profile' => request()->auth_user,
            'broker_profile_url' => $this->broker->getServerURL(config('master.uri.profile')),
            'broker_logout_url' => $this->broker->getServerURL(config('master.uri.logout'), ['follow' => true]),
        ]);
    }

}
