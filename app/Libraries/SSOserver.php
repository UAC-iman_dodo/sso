<?php

namespace App\Libraries;

use App\Broker;
use App\User;
use App\Level;
use Jasny\SSO\Server;
use Jasny\ValidationResult;
use Illuminate\Validation\Validator;

class SSOserver extends Server
{
    public function __construct()
    {
        parent::__construct();
    }

    public function logout()
    {
        $this->startBrokerSession();
        $this->setSessionData('sso_user', null);
        return true;
    }

    public function userInfo()
    {
        $this->startBrokerSession();
        $user = null;

        $username = $this->getSessionData('sso_user');

        if ($username) {
            $user = $this->getUserInfo($username);
            $user['error'] = 'user not found';
            if (!$user) return $this->fail("User not found", 500); // Shouldn't happen
        }
        return response()->json($user, 200);
    }

    public function brokerInfo()
    {
        $this->startBrokerSession();
        $broker = $this->getBrokerInfo($this->brokerId);
        return $broker;
    }

    public function attach()
    {
        $request = request();

        $this->detectReturnType();

        if (!$request->filled('broker')) {
            return $this->fail("Broker empty", 400);
        }
        if (!$request->filled('token')) {
            return $this->fail("token empty", 400);
        }
        if (!$this->returnType) {
            return $this->fail("server error return type", 400);
        }

        $checksum = $this->generateAttachChecksum($request->input('broker'), $request->input('token'));

        $reqChecksum = $request->input('checksum');
        if (empty($reqChecksum) || $checksum != $reqChecksum) {
            return $this->fail("invalid check sum", 400);
        }

        $this->startUserSession();
        $sid = $this->generateSessionId($_REQUEST['broker'], $_REQUEST['token']);

        $this->cache->set($sid, $this->getSessionData('id'));
        return $this->outputAttachSuccess();
    }

    protected function outputAttachSuccess()
    {
        if ($this->returnType === 'image') {
            $this->outputImage();
        }

        if ($this->returnType === 'json') {
            return response()->json(['success' => attached]);
        }

        if ($this->returnType === 'jsonp') {
            return response()->jsonp($_REQUEST['callback'], ['success' => 'attached'], 200);
        }

        if ($this->returnType === 'redirect') {
            // $url = $_REQUEST['return_url'];
            // header("Location: $url", true, 307);
            // echo "You're being redirected to <a href='{$url}'>$url</a>";
            $url = $_REQUEST['return_url']; // request()->input('return_url', null);
            return redirect()->away($url, 307);
        }
    }

    public function authenticate($email, $password)
    {
        if (!isset($email)) {
            return ValidationResult::error("email kosongs");
        }
        if (!isset($password)) {
            return ValidationResult::error("password kosong");
        }

        $user = User::where('email', $email)->first();
        if (!$user || !$user->checkPassword($password)) {
            return ValidationResult::error("password salah");
        }

        $this->setSessionData('sso_user', $user->id);
        // $data['user'] = $user;
        // $data['status'] = ValidationResult::success();
        return ValidationResult::success();
        // return $user;
    }

    protected function getBrokerInfo($brokerId)
    {
        $broker = Broker::where([
            ['share_id', '=', $brokerId],
            ['status', '=', Broker::BROKER_STATUS['active']]
        ])->first();
        return !empty($broker) ? $broker->toArray() : null;
    }

    protected function getUserInfo($id)
    {
        $user = User::with('levels')->find($id);
        return !empty($user) ? $user->toArray() : null;
    }

    public function email()
    {
        $this->startBrokerSession();
        $data = User::select('id','email')
                ->where('id','!=','')
                ->get();
        $test = [];
        $statusCode = 0;

        if ($data) {
            $statusCode = 200;
            foreach ($data as $datas) {
                $array = [
                    'id'=> $datas->id,
                    'email'  => $datas->email
                ];
                array_push($test, $array);
            }

            $response =  [
                'data'  => $test
            ];

        }
        else{
            $response = 'Data Null';
            $statusCode = 400;
        }
            
            $Broker = new Broker();
            dd($Broker->check());
        return response()->json($response, $statusCode);
    }
}
