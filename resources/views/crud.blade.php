@extends('tempt')

@section('content')

<div class="content">
    
	<div class="container-fluid">

			<div class="row">
				<div class="col-xl-12">
						<div class="breadcrumb-holder">
                                <h1 class="main-title float-left">User</h1>
                                <ol class="breadcrumb float-right">
									<li class="breadcrumb-item active">{{$subtitle}}</li>
                                </ol>
                                <div class="clearfix"></div>
                        </div>
				</div>
			</div>
			<div class="row">			
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">						
					<div class="card mb-3">
						<div class="card-header">
						</div>
						<form action="{{$action}}" method="post">
						{{ csrf_field() }}
						<div class="card-body">
							<div class="form-group">
								<label for="name">Nama</label>
								<input class="form-control" id="name" name="name" required="" type="text" value="{{ @$user->name }}">
							</div>
							<div class="form-group">
								<label for="sender">Email</label>
								<input class="form-control" id="sender" name="email"  required="" type="text" value="{{ @$user->email }}">
							</div>
							<div class="form-group" style="{{ !empty($user)?'display: none':'' }}">
								<label for="receiver">Password</label>
								<input class="form-control" id="receiver" name="password"  required="" type="password" value="">
							</div>
							<div class="form-group">
								<label for="level">Level</label>
								{{ Form::select('level', $level, @$user->level_id, ['class'=>'form-control', 'id'=>'level', 'type'=>'text'])}}
								<!-- <input class="form-control" id="level" name="level" required="" type="text" value="{{ @$user->level }}"> -->
							</div>
							<div class="form-group">
								<label for="targets">Nomor telepon</label>
								<input class="form-control" id="targets" name="no_telp"  required="" type="text" value="{{ @$user->no_telp }}">
						</div>
						  	<button type="submit" name="submit" class="btn btn-primary">Submit</button>
						</form>					
					</div><!-- end card-->					
                </div>
			</div>
    </div>
	<!-- END container-fluid -->

</div>
@endsection