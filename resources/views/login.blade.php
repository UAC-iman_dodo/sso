<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Login</title>

	<link rel="shortcut icon" type="image/png" href="{{ asset('pike/images/logo.png') }}"/>
    <!-- Core CSS -->
    <link href="{{ asset('pike/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('pike/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">

	<!-- Custom CSS -->
	<link href="{{ asset('pike/css/login.css') }}" rel="stylesheet">

    <!-- Checkboxes style -->
    <link href="{{ asset('pike/css/bootstrap-checkbox.css') }}" rel="stylesheet">
</head>


<body>

<div class="login-menu">
      <div class="container">
        <nav class="nav">
          <a class="nav-link active"></a>
        </nav>
      </div>
</div>

<div class="container h-100">
	<div class="row h-100 justify-content-center align-items-center">
				
						
		<div class="card">
			<h4 class="card-header">Login</h4>
           
			<div class="card-body">
			
                <form data-toggle="validator" role="form" method="post" action="{{route('login',$params)}}">                                
						
						{{ csrf_field() }}
						<div class="row">	
							<div class="col-md-12">    
								<div class="form-group">
								<label>Username / Email</label>
								<div class="input-group">
								  <div class="input-group-prepend">
									<span class="input-group-text" id="basic-addon1"><i class="fa fa-envelope-open-o" aria-hidden="true"></i></span>
									</div>
								  <input type="email" class="form-control" name="email" data-error="Input valid email" required>								  
								</div>								
							<div class="help-block with-errors text-danger"></div>
							</div>
							</div>
                        </div>
						
						<div class="row">								
							<div class="col-md-12">
								<div class="form-group">
								<label>Password</label>
								<div class="input-group">
									<div class="input-group-prepend">
									<span class="input-group-text" id="basic-addon1"><i class="fa fa-unlock" aria-hidden="true"></i></span>
									</div>
									<input type="password" id="inputPassword" data-minlength="6" name="password" class="form-control" data-error="Password to short" required />
								</div>	
								<div class="help-block with-errors text-danger"></div>
								</div>
							</div>
						</div>
						
                        <div class="row">
							<div class="col-md-12">
							<input type="hidden" name="redirect" value="" />
							<input type="submit" class="btn btn-primary btn-lg btn-block" value="Login" name="submit" />
							</div>
						</div>
                </form>

                <div class="clear"></div> 
						
			</div>	
			
		</div>	
		
	</div>	
</div>
	
<!-- Core Scripts -->
<script src="{{ asset('pike/js/jquery-1.10.2.min.js') }}"></script>
<script src="{{ asset('pike/js/bootstrap.min.js') }}"></script>
	

<!-- Bootstrap validator  -->
<!-- <script src="{{ asset('pike/js/validator.min.js') }}"></script> -->
</body>
</html>	